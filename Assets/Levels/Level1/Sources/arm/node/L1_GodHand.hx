package arm.node;

@:keep class L1_GodHand extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _SetVisible = new armory.logicnode.SetVisibleNode(this);
		var _SetLocation_007 = new armory.logicnode.SetLocationNode(this);
		var _SetLocation_004 = new armory.logicnode.SetLocationNode(this);
		var _SetLocation_002 = new armory.logicnode.SetLocationNode(this);
		var _SetLocation_001 = new armory.logicnode.SetLocationNode(this);
		var _RemoveObject = new armory.logicnode.RemoveObjectNode(this);
		var _OnInit = new armory.logicnode.OnInitNode(this);
		_OnInit.addOutputs([_RemoveObject]);
		_RemoveObject.addInput(_OnInit, 0);
		_RemoveObject.addInput(new armory.logicnode.ObjectNode(this, "Env.Coin1"), 0);
		_RemoveObject.addOutputs([_SetLocation_001]);
		_SetLocation_001.addInput(_RemoveObject, 0);
		_SetLocation_001.addInput(new armory.logicnode.ObjectNode(this, "L1.Level.Secret1.Part2"), 0);
		_SetLocation_001.addInput(new armory.logicnode.VectorNode(this, -5000.0, 0.0, -6.0), 0);
		_SetLocation_001.addOutputs([_SetLocation_002]);
		_SetLocation_002.addInput(_SetLocation_001, 0);
		_SetLocation_002.addInput(new armory.logicnode.ObjectNode(this, "69NewsClassic"), 0);
		_SetLocation_002.addInput(new armory.logicnode.VectorNode(this, -5000.0, 0.0, -2.0), 0);
		_SetLocation_002.addOutputs([_SetLocation_004]);
		_SetLocation_004.addInput(_SetLocation_002, 0);
		_SetLocation_004.addInput(new armory.logicnode.ObjectNode(this, "STD"), 0);
		_SetLocation_004.addInput(new armory.logicnode.VectorNode(this, -5020.0, 0.0, -2.0), 0);
		_SetLocation_004.addOutputs([_SetLocation_007]);
		_SetLocation_007.addInput(_SetLocation_004, 0);
		_SetLocation_007.addInput(new armory.logicnode.ObjectNode(this, "SV.Black"), 0);
		_SetLocation_007.addInput(new armory.logicnode.VectorNode(this, -5005.0, 0.0, -2.0), 0);
		_SetLocation_007.addOutputs([_SetVisible]);
		_SetVisible.addInput(_SetLocation_007, 0);
		_SetVisible.addInput(new armory.logicnode.ObjectNode(this, "L1.Secret2"), 0);
		_SetVisible.addInput(new armory.logicnode.BooleanNode(this, false), 0);
		_SetVisible.addOutputs([new armory.logicnode.NullNode(this)]);
		var _SetVisible_001 = new armory.logicnode.SetVisibleNode(this);
		var _OnTimer = new armory.logicnode.OnTimerNode(this);
		_OnTimer.addInput(new armory.logicnode.FloatNode(this, 0.5), 0);
		_OnTimer.addInput(new armory.logicnode.BooleanNode(this, false), 0);
		_OnTimer.addOutputs([_SetVisible_001]);
		_SetVisible_001.addInput(_OnTimer, 0);
		_SetVisible_001.addInput(new armory.logicnode.ObjectNode(this, "L1.Secret2"), 0);
		_SetVisible_001.addInput(new armory.logicnode.BooleanNode(this, false), 0);
		_SetVisible_001.addOutputs([new armory.logicnode.NullNode(this)]);
		var _SetVisible_002 = new armory.logicnode.SetVisibleNode(this);
		var _OnContact = new armory.logicnode.OnContactNode(this);
		_OnContact.property0 = "Begin";
		_OnContact.addInput(new armory.logicnode.ObjectNode(this, "L1.Secret2.Trigger"), 0);
		_OnContact.addInput(new armory.logicnode.ObjectNode(this, "L1.Player"), 0);
		_OnContact.addOutputs([_SetVisible_002]);
		_SetVisible_002.addInput(_OnContact, 0);
		_SetVisible_002.addInput(new armory.logicnode.ObjectNode(this, "L1.Secret2"), 0);
		_SetVisible_002.addInput(new armory.logicnode.BooleanNode(this, true), 0);
		_SetVisible_002.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
