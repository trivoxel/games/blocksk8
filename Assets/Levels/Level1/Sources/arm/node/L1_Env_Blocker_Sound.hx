package arm.node;

@:keep class L1_Env_Blocker_Sound extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _PlaySpeaker = new armory.logicnode.PlaySoundNode(this);
		var _OnEvent = new armory.logicnode.OnEventNode(this);
		_OnEvent.property0 = "door";
		_OnEvent.addOutputs([_PlaySpeaker]);
		_PlaySpeaker.addInput(_OnEvent, 0);
		_PlaySpeaker.addInput(new armory.logicnode.ObjectNode(this, "L1.Env.Blocker.Sound"), 0);
		_PlaySpeaker.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
