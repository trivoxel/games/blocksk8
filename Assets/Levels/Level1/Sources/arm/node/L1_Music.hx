package arm.node;

@:keep class L1_Music extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _OnInit = new armory.logicnode.OnInitNode(this);
		_OnInit.addOutputs([new armory.logicnode.NullNode(this)]);
		var _PlaySpeaker_001 = new armory.logicnode.PlaySoundNode(this);
		var _StopSpeaker_001 = new armory.logicnode.StopSoundNode(this);
		var _OnKeyboard = new armory.logicnode.OnKeyboardNode(this);
		_OnKeyboard.property0 = "Started";
		_OnKeyboard.property1 = "p";
		_OnKeyboard.addOutputs([_StopSpeaker_001]);
		_StopSpeaker_001.addInput(_OnKeyboard, 0);
		_StopSpeaker_001.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_StopSpeaker_001.addOutputs([_PlaySpeaker_001]);
		_PlaySpeaker_001.addInput(_StopSpeaker_001, 0);
		_PlaySpeaker_001.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_PlaySpeaker_001.addOutputs([new armory.logicnode.NullNode(this)]);
		var _StopSpeaker = new armory.logicnode.StopSoundNode(this);
		var _OnKeyboard_001 = new armory.logicnode.OnKeyboardNode(this);
		_OnKeyboard_001.property0 = "Started";
		_OnKeyboard_001.property1 = "m";
		_OnKeyboard_001.addOutputs([_StopSpeaker]);
		_StopSpeaker.addInput(_OnKeyboard_001, 0);
		_StopSpeaker.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_StopSpeaker.addOutputs([new armory.logicnode.NullNode(this)]);
		var _PlaySpeaker = new armory.logicnode.PlaySoundNode(this);
		_PlaySpeaker.addInput(new armory.logicnode.NullNode(this), 0);
		_PlaySpeaker.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_PlaySpeaker.addOutputs([new armory.logicnode.NullNode(this)]);
		var _PlaySpeaker_002 = new armory.logicnode.PlaySoundNode(this);
		var _StopSpeaker_002 = new armory.logicnode.StopSoundNode(this);
		var _OnTimer = new armory.logicnode.OnTimerNode(this);
		_OnTimer.addInput(new armory.logicnode.FloatNode(this, 0.5), 0);
		_OnTimer.addInput(new armory.logicnode.BooleanNode(this, false), 0);
		_OnTimer.addOutputs([_StopSpeaker_002]);
		_StopSpeaker_002.addInput(_OnTimer, 0);
		_StopSpeaker_002.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_StopSpeaker_002.addOutputs([_PlaySpeaker_002]);
		_PlaySpeaker_002.addInput(_StopSpeaker_002, 0);
		_PlaySpeaker_002.addInput(new armory.logicnode.ObjectNode(this, "L1.Music"), 0);
		_PlaySpeaker_002.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
