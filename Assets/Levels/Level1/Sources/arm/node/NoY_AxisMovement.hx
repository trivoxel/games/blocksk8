package arm.node;

@:keep class NoY_AxisMovement extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _SetLocation = new armory.logicnode.SetLocationNode(this);
		var _OnUpdate = new armory.logicnode.OnUpdateNode(this);
		_OnUpdate.addOutputs([_SetLocation]);
		_SetLocation.addInput(_OnUpdate, 0);
		_SetLocation.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		var _Vector = new armory.logicnode.VectorNode(this);
		var _SeparateXYZ = new armory.logicnode.SeparateVectorNode(this);
		var _GetLocation = new armory.logicnode.GetLocationNode(this);
		_GetLocation.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_GetLocation.addOutputs([_SeparateXYZ]);
		_SeparateXYZ.addInput(_GetLocation, 0);
		_SeparateXYZ.addOutputs([_Vector]);
		_SeparateXYZ.addOutputs([new armory.logicnode.FloatNode(this, 0.0)]);
		_SeparateXYZ.addOutputs([_Vector]);
		_Vector.addInput(_SeparateXYZ, 0);
		_Vector.addInput(new armory.logicnode.FloatNode(this, 0.0), 0);
		_Vector.addInput(_SeparateXYZ, 2);
		_Vector.addOutputs([_SetLocation]);
		_SetLocation.addInput(_Vector, 0);
		_SetLocation.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
