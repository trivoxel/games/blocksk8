package arm.node;

@:keep class L1_WinTextTemp extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _OnEvent = new armory.logicnode.OnEventNode(this);
		_OnEvent.property0 = "finish";
		_OnEvent.addOutputs([new armory.logicnode.NullNode(this)]);
		var _SpawnObject = new armory.logicnode.SpawnObjectNode(this);
		_SpawnObject.addInput(new armory.logicnode.NullNode(this), 0);
		_SpawnObject.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		var _Transform = new armory.logicnode.TransformNode(this);
		var _VectorMath = new armory.logicnode.VectorMathNode(this);
		_VectorMath.property0 = "Subtract";
		var _GetLocation = new armory.logicnode.GetLocationNode(this);
		_GetLocation.addInput(new armory.logicnode.ObjectNode(this, "L1.Player.Camera"), 0);
		_GetLocation.addOutputs([_VectorMath]);
		_VectorMath.addInput(_GetLocation, 0);
		_VectorMath.addInput(new armory.logicnode.VectorNode(this, 0.0, 6.0, 0.0), 0);
		_VectorMath.addOutputs([_Transform]);
		_VectorMath.addOutputs([new armory.logicnode.FloatNode(this, 0.0)]);
		_Transform.addInput(_VectorMath, 0);
		var _GetRotation = new armory.logicnode.GetRotationNode(this);
		_GetRotation.addInput(new armory.logicnode.ObjectNode(this, "L1.WinTextTemp"), 0);
		_GetRotation.addOutputs([_Transform]);
		_Transform.addInput(_GetRotation, 0);
		_Transform.addInput(new armory.logicnode.VectorNode(this, 1.0, 1.0, 1.0), 0);
		_Transform.addOutputs([_SpawnObject]);
		_SpawnObject.addInput(_Transform, 0);
		_SpawnObject.addOutputs([new armory.logicnode.NullNode(this)]);
		_SpawnObject.addOutputs([new armory.logicnode.ObjectNode(this, "")]);
	}
}
