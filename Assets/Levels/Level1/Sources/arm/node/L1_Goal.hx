package arm.node;

@:keep class L1_Goal extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _SendEvent = new armory.logicnode.SendEventNode(this);
		_SendEvent.addInput(new armory.logicnode.NullNode(this), 0);
		_SendEvent.addInput(new armory.logicnode.StringNode(this, "finish"), 0);
		_SendEvent.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_SendEvent.addOutputs([new armory.logicnode.NullNode(this)]);
		var _SetScene = new armory.logicnode.SetSceneNode(this);
		var _OnContact = new armory.logicnode.OnContactNode(this);
		_OnContact.property0 = "Begin";
		_OnContact.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_OnContact.addInput(new armory.logicnode.ObjectNode(this, "L1.Player"), 0);
		_OnContact.addOutputs([_SetScene]);
		_SetScene.addInput(_OnContact, 0);
		var _Scene = new armory.logicnode.SceneNode(this);
		_Scene.property0 = "";
		_Scene.addOutputs([_SetScene]);
		_SetScene.addInput(_Scene, 0);
		_SetScene.addOutputs([new armory.logicnode.NullNode(this)]);
		_SetScene.addOutputs([new armory.logicnode.ObjectNode(this, "")]);
	}
}
