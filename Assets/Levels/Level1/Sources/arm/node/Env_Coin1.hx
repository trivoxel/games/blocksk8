package arm.node;

@:keep class Env_Coin1 extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _RotateObject = new armory.logicnode.RotateObjectNode(this);
		var _OnUpdate = new armory.logicnode.OnUpdateNode(this);
		_OnUpdate.addOutputs([_RotateObject]);
		_RotateObject.addInput(_OnUpdate, 0);
		_RotateObject.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_RotateObject.addInput(new armory.logicnode.VectorNode(this, 0.0, 0.0, 0.07500000298023224), 0);
		_RotateObject.addOutputs([new armory.logicnode.NullNode(this)]);
		var _SendGlobalEvent = new armory.logicnode.SendGlobalEventNode(this);
		var _IsTrue = new armory.logicnode.IsTrueNode(this);
		var _OnUpdate_001 = new armory.logicnode.OnUpdateNode(this);
		_OnUpdate_001.addOutputs([_IsTrue]);
		_IsTrue.addInput(_OnUpdate_001, 0);
		var _HasContact = new armory.logicnode.HasContactNode(this);
		_HasContact.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_HasContact.addInput(new armory.logicnode.ObjectNode(this, "L1.Player"), 0);
		_HasContact.addOutputs([_IsTrue]);
		_IsTrue.addInput(_HasContact, 0);
		var _RemoveObject = new armory.logicnode.RemoveObjectNode(this);
		_RemoveObject.addInput(_IsTrue, 0);
		_RemoveObject.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_RemoveObject.addOutputs([new armory.logicnode.NullNode(this)]);
		_IsTrue.addOutputs([_RemoveObject, _SendGlobalEvent]);
		_SendGlobalEvent.addInput(_IsTrue, 0);
		_SendGlobalEvent.addInput(new armory.logicnode.StringNode(this, "Score+1"), 0);
		_SendGlobalEvent.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
