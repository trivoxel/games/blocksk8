package arm.node;

@:keep class L1_Coin1Spawn extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _SpawnObject = new armory.logicnode.SpawnObjectNode(this);
		var _OnTimer = new armory.logicnode.OnTimerNode(this);
		_OnTimer.addInput(new armory.logicnode.FloatNode(this, 0.10000000149011612), 0);
		_OnTimer.addInput(new armory.logicnode.BooleanNode(this, false), 0);
		_OnTimer.addOutputs([_SpawnObject]);
		_SpawnObject.addInput(_OnTimer, 0);
		_SpawnObject.addInput(new armory.logicnode.ObjectNode(this, "Env.Coin1"), 0);
		var _Transform = new armory.logicnode.TransformNode(this);
		var _GetLocation = new armory.logicnode.GetLocationNode(this);
		_GetLocation.addInput(new armory.logicnode.ObjectNode(this, ""), 0);
		_GetLocation.addOutputs([_Transform]);
		_Transform.addInput(_GetLocation, 0);
		_Transform.addInput(new armory.logicnode.VectorNode(this, 0.0, 0.0, 0.0), 0);
		_Transform.addInput(new armory.logicnode.VectorNode(this, 1.0, 1.0, 1.0), 0);
		_Transform.addOutputs([_SpawnObject]);
		_SpawnObject.addInput(_Transform, 0);
		_SpawnObject.addOutputs([new armory.logicnode.NullNode(this)]);
		_SpawnObject.addOutputs([new armory.logicnode.ObjectNode(this, "")]);
	}
}
