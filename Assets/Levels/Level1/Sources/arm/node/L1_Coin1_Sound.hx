package arm.node;

@:keep class L1_Coin1_Sound extends armory.logicnode.LogicTree {

	public function new() { super(); notifyOnAdd(add); }

	override public function add() {
		var _PlaySpeaker = new armory.logicnode.PlaySoundNode(this);
		var _OnEvent = new armory.logicnode.OnEventNode(this);
		_OnEvent.property0 = "Score+1";
		_OnEvent.addOutputs([_PlaySpeaker]);
		_PlaySpeaker.addInput(_OnEvent, 0);
		_PlaySpeaker.addInput(new armory.logicnode.ObjectNode(this, "L1.Coin1.Sound"), 0);
		_PlaySpeaker.addOutputs([new armory.logicnode.NullNode(this)]);
	}
}
